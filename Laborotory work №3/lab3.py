import matplotlib.pyplot as plt
import math


while True:
    my_function = input('Функция, которую вы хотите посчитать: ')
    if my_function in ['g', 'f', 'y']:
        pass
    else:
        print('Неправильное выбор функции!\n')
        continue

    # Создание массивов для хранения значений x и y
    x_list = []
    y_list = []

    # Объявление переменных

    x = a = x_max = x_step = 0

    try:
        x = float(input('Введите значение переменной x для функции: '))
        a = float(input('Введите значение переменной a для функции: '))
        x_max = float(input('Введите максимальное значение x: '))
        x_step = float(input('Введите количество шагов: '))
    except ValueError:
        print('Вы ввели неверное значение ("ValueError")')

    # Подсчет функции g

    if my_function == 'g':
        while x < x_max:
            try:
                g = 3 * (3 * a ** 2 - 12 * a * x + 4 * x ** 2) / (54 * a ** 2 + 87 * a * x + 35 * x ** 2)
                x_list.append(x), y_list.append(g)
                x += x_step
            except ZeroDivisionError:
                x_list.append(x), y_list.append(None)

    # Подсчет функции f

    if my_function == 'f':
        while x < x_max:
            try:
                f = math.cosh(4 * a**2 - x**2)
                x_list.append(x), y_list.append(f)
                x += x_step
            except OverflowError or ValueError:
                x_list.append(x), y_list.append(None)

    # Подсчет функции y

    if my_function == 'y':
        if x >= -1:
            while x < x_max:
                try:
                    y = math.asin(2 * a ** 2 - 23 * a * x + 45 * x ** 2)
                    x_list.append(x), y_list.append(y)
                    x += x_step
                except ValueError:
                    x_list.append(x), y_list.append(None)

    print(x_list)
    print(y_list)

    # Построение графика для выбранной ранее функции

    plt.plot(x_list, y_list, 'k')
    plt.axis('tight')
    plt.show()

    re = input('Если вы хотите выйти, нажмите yes, в ином случае наберите любое другое значение')
    if re == 'yes':
        break
    else:
        print('Программа повторяется заново')
        continue
