import matplotlib.pyplot as plt
import math


while True:

    # Выбор функции для подсчета

    my_function = input('Функция, которую вы хотите посчитать: ')
    if my_function in ['g', 'f', 'y']:
        pass
    else:
        print('Неправильное выбор функции!\n')
        continue

    # Создание массивов для хранения значений x и y

    x_list = []
    y_list = []

    # Объявление переменных

    x = a = x_max = x_step = search = 0

    # Проверка значений, введенных пользователем
    
    try:
        x = float(input('Введите значение переменной x для функции: '))
        a = float(input('Введите значение переменной a для функции: '))
        x_max = float(input('Введите максимальное значение x: '))
        x_step = float(input('Введите количество шагов: '))
    except ValueError:
        print('Вы ввели неверное значение ("ValueError")')

    # Подсчет функции g

    if my_function == 'g':
        while x < x_max:
            try:
                g = 3 * (3 * a ** 2 - 12 * a * x + 4 * x ** 2) / (54 * a ** 2 + 87 * a * x + 35 * x ** 2)
                x_list.append(x), y_list.append(g)
                x += x_step
                print(f'x = {x} \t y = {g}\n')
            except ZeroDivisionError:
                x_list.append(x), y_list.append(None)
                print(f'x = {x} \t y = {None}')

    # Подсчет функции f

    if my_function == 'f':
        while x < x_max:
            try:
                f = math.cosh(4 * a**2 - x**2)
                x_list.append(x), y_list.append(f)
                x += x_step
                print()
                print(f'x = {x}, y = {f}\n')
            except OverflowError or ValueError:
                x_list.append(x), y_list.append(None)
                print(f'x = {x} \t y = {None}')

    # Подсчет функции y

    if my_function == 'y':
        if x >= -1:
            while x < x_max:
                try:
                    y = math.asin(2 * a ** 2 - 23 * a * x + 45 * x ** 2)
                    x_list.append(x), y_list.append(y)
                    x += x_step
                    print()
                    print(f'x = {x} \t y = {y}\n')
                except ValueError:
                    x_list.append(x), y_list.append(None)
                    print(f'x = {x} \t y = {None}')

    print(x_list)
    print(y_list)

    # Построение графика для выбранной ранее функции

    plt.plot(x_list, y_list, 'k')
    plt.axis('tight')
    plt.show()
    y_list = list(filter(None, y_list))

    # Вывод минимальных и максимальных значений в x_list и y_list

    print('Минимальный элемент x_list:', min(x_list))
    print('Максимальный элемент x_list:', max(x_list))
    print('Минимальный элемент y_list:', min(y_list))
    print('Максимальный элемент y_list:', max(y_list))

    # Вывод количества совпадений числа, которое искал пользователь
    search = float(input('Число, которое вы хотите найти:\n'))

    print(f"{', '.join([str(e) for e in x_list])}")
    print(f"{', '.join([str(e) for e in y_list])}")
    result_x = x_list.count(search)
    result_y = y_list.count(search)

    # Вывод найденных совпадений в x_list и y_list

    print(f'Найденно совпадений в списке x: {result_x}')
    print(f'Найденно совпадений в списке y: {result_y}')

    # счет количества четных цифр в введеном числе, если оно целое, иначе выводится 0

    number = input('Введите число для определения в нем количества четных цифр:\n')
    count = 0
    if number == int(number):
        while number != 0:
            if number % 2 == 0:
                count += 1
            number = number // 10
        print(f'Количество четных цифр: {count}')
    else:
        print('число нецелое', 0)

    # Реализация завершения программы или ее повтора

    re = input('Если вы хотите выйти, нажмите yes, в ином случае наберите любое другое значение\n')
    if re == 'yes':
        break
    else:
        print('Программа повторяется заново')
        continue
