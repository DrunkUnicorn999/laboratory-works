import math


def g_func(x, a):
    return 3 * (3 * a ** 2 - 12 * a * x + 4 * x ** 2) / (54 * a ** 2 + 87 * a * x + 35 * x ** 2)


def f_func(x, a):
    return math.cosh(4 * a ** 2 - x ** 2)


def y_func(x, a):
    return math.asin(2 * a ** 2 - 23 * a * x + 45 * x ** 2)
